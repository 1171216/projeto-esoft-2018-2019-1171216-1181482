/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author brunosantos
 */
public class HabilitacaoProfissional {
    private String descricao;
    private String grau;
    private String classificacao;

    public HabilitacaoProfissional(String descricao, String grau, String classificacao) {
        this.descricao = descricao;
        this.grau = grau;
        this.classificacao = classificacao;
    }
    
}
