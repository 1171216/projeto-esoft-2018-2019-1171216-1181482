/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author brunosantos
 */
public class PrestadorServico {
    private String numero;
    private String nome;
    private String abrev;
    private String nif;
    private String email;

    public PrestadorServico(String numero, String nome, String abrev, String nif, String email) {
        this.numero = numero;
        this.nome = nome;
        this.abrev = abrev;
        this.nif = nif;
        this.email = email;
    }
    public void addCategoria(String cat){}
    public void addAreaGeografica(String ag){}
    public void getNome(){}
    public void getEmail(){}
    public void getListaDisponiblidades(){}
}
