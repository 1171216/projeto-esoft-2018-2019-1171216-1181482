/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author brunosantos
 */
public class Disponiblidade {
    private String dataInicio;
    private String horaInicio;
    private String dataFim;
    private String horaFim;

    public Disponiblidade(String dataInicio, String horaInicio, String dataFim, String horaFim) {
        this.dataInicio = dataInicio;
        this.horaInicio = horaInicio;
        this.dataFim = dataFim;
        this.horaFim = horaFim;
    }
    
}
