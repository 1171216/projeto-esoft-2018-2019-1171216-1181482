/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author brunosantos
 */
public class ServicoLimitado {
    private String id;
    private String descBreve;
    private String descCompleta;
    private String custoHora;

    public ServicoLimitado(String id, String descBreve, String descCompleta, String custoHora) {
        this.id = id;
        this.descBreve = descBreve;
        this.descCompleta = descCompleta;
        this.custoHora = custoHora;
    }
    
}
